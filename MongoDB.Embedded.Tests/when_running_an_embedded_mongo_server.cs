﻿using System.Data.Common;
using MongoDB.Bson;
using NUnit.Framework;

namespace MongoDB.Embedded.Tests
{
    [TestFixture]
    public class when_running_an_embedded_mongo_server
    {
        [Test]
        public void documents_can_be_saved()
        {
            using (var mongo = new EmbeddedMongoServer())
            {
                mongo.Start();

                var server = mongo.GetServer();
                var database = server.GetDatabase("test");
                var docs = database.GetCollection<TestDoc>("tests");

                docs.Save(new TestDoc {Name = "test doc 1"});
            }
        }

        public class TestDoc
        {
            public ObjectId Id { get; set; }
            public string Name { get; set; }
        }
    }
}