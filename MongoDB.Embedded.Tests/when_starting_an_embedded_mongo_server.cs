﻿using System.Diagnostics;
using System.Linq;
using NUnit.Framework;

namespace MongoDB.Embedded.Tests
{
    [TestFixture]
    public class when_starting_an_embedded_mongo_server
    {
        [Test]
        public void the_mongo_process_runs()
        {
            using (var mongo = new EmbeddedMongoServer())
            {
                mongo.Start();
                Assert.IsNotNull(Process.GetProcessesByName("mongod").FirstOrDefault());
            }
        }
    }
}
