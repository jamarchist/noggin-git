﻿using System;
using System.Collections.Generic;
using System.IO;

namespace MongoDB.Embedded
{
    internal class DefaultEmbeddedMongoServerSettings : IEmbeddedMongoServerSettings
    {
        public DefaultEmbeddedMongoServerSettings()
        {
            MongoExecutablePath = Path.Combine(Path.GetTempPath(), "Mongo");
            MongoConfigurationPath = MongoExecutablePath;
            MongoDatabaseLocation = Path.Combine(MongoExecutablePath, "db", "data");
            AdditionalCommandLineParameters = String.Empty;
            Port = 27017;
            ServerProvider = new DefaultMongoServerProvider();
            StartupTasks = new List<IStartupTask>();
        }

        public string MongoExecutablePath { get; set; }
        public string MongoConfigurationPath { get; set; }
        public string MongoDatabaseLocation { get; set; }
        public string AdditionalCommandLineParameters { get; set; }
        public bool ShowWindow { get; set; }
        public bool StartClean { get; set; }
        public int Port { get; set; }
        public IMongoServerProvider ServerProvider { get; set; }
        public IList<IStartupTask> StartupTasks { get; set; }
    }
}