﻿using MongoDB.Driver;

namespace MongoDB.Embedded
{
    public class DefaultMongoServerProvider : IMongoServerProvider
    {
        public MongoServer GetServer(IMongoServerProviderContext context)
        {
            return context.Client.GetServer();
        }
    }
}