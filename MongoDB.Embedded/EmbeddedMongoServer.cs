﻿using System;
using System.Diagnostics;
using System.IO;
using System.Linq;
using MongoDB.Driver;

namespace MongoDB.Embedded
{
    public class EmbeddedMongoServer : IDisposable
    {
        public EmbeddedMongoServer()
        {
            Settings = new DefaultEmbeddedMongoServerSettings();
        }

        public EmbeddedMongoServer(IEmbeddedMongoServerSettings settings)
        {
            Settings = settings;
        }

        public void Start()
        {
            EnsureDatabaseDirectoryExists();
            EnsureMongoExeExists(Settings.MongoExecutablePath);
            EnsureMongoConfExists(Settings.MongoConfigurationPath);
            StartMongoExe();
            Connect();            
        }

        public MongoServer GetServer()
        {
            return Settings.ServerProvider.GetServer(new MongoServerProviderContext
            {
                Client = Client
            });
        }

        public IEmbeddedMongoServerSettings Settings { get; private set; }
        public MongoClient Client { get; private set; }
        private MongoServer Server { get; set; }
        private int MongoProcessId { get; set; }

        private void EnsureMongoExeExists(string exeDirectory)
        {
            var mongoPath = Path.Combine(exeDirectory, "mongod.exe");
            if (!File.Exists(mongoPath))
            {
                File.WriteAllBytes(mongoPath, MongoResources.mongod);
            }
        }

        private void EnsureMongoConfExists(string confDirectory)
        {
            var confPath = Path.Combine(confDirectory, "mongod.conf");
            if (!File.Exists(confPath))
            {
                File.WriteAllBytes(confPath, MongoResources.mongodconf);
            }
        }

        private void EnsureDatabaseDirectoryExists()
        {
            if (!Directory.Exists(Settings.MongoDatabaseLocation))
            {
                Directory.CreateDirectory(Settings.MongoDatabaseLocation);
            }
        }

        private void StartMongoExe()
        {
            var runningMongo = Process.GetProcessesByName("mongod").Any();
            if (!runningMongo)
            {
                var commandLocation = Path.Combine(Settings.MongoExecutablePath, "mongod.exe");
                var configLocation = Path.Combine(Settings.MongoConfigurationPath, "mongod.conf");

                Debug.WriteLine(String.Format("Starting Mongo at '{0}'", commandLocation));

                var startInfo = new ProcessStartInfo();
                if (!Settings.ShowWindow)
                {
                    startInfo.UseShellExecute = false;
                    startInfo.CreateNoWindow = true;
                    startInfo.WindowStyle = ProcessWindowStyle.Hidden;
                }

                startInfo.FileName = commandLocation;
                startInfo.Arguments = String.Format("--rest --dbpath \"{0}\" --port {1} --config \"{2}\"",
                    Settings.MongoDatabaseLocation,
                    Settings.Port,
                    configLocation);
                var mongoProcess = Process.Start(startInfo);
                MongoProcessId = mongoProcess.Id;
            }
        }

        private void Connect()
        {
            Client = new MongoClient(new MongoUrl(String.Format(@"mongodb://localhost:{0}/", Settings.Port)));
            Server = Client.GetServer();

            if (Settings.StartClean)
            {
                foreach (var db in Server.GetDatabaseNames())
                {
                    Server.DropDatabase(db);
                }
            }

            foreach (var startupTask in Settings.StartupTasks)
            {
                startupTask.Execute(new StartupContext
                {
                    BaseServer = Server,
                    Client = Client,
                    ModifiedServer = GetServer()
                });
            }
        }

        public void Dispose()
        {
            if (MongoProcessId != 0)
            {
                var mongoProcess = Process.GetProcessById(MongoProcessId);
                Server.Shutdown();
                mongoProcess.WaitForExit();                
            }
        }
    }
}
