﻿using System.Collections.Generic;

namespace MongoDB.Embedded
{
    public interface IEmbeddedMongoServerSettings
    {
        string MongoExecutablePath { get; set; }
        string MongoConfigurationPath { get; set; }
        string MongoDatabaseLocation { get; set; }
        string AdditionalCommandLineParameters { get; set; }
        bool ShowWindow { get; set; }
        bool StartClean { get; set; }
        int Port { get; set; }
        IMongoServerProvider ServerProvider { get; set; }
        IList<IStartupTask> StartupTasks { get; set; }
    }
}
