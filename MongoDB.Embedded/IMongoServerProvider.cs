﻿using MongoDB.Driver;

namespace MongoDB.Embedded
{
    public interface IMongoServerProvider
    {
        MongoServer GetServer(IMongoServerProviderContext context);
    }
}