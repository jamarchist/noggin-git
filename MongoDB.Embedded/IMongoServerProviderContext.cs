﻿using MongoDB.Driver;

namespace MongoDB.Embedded
{
    public interface IMongoServerProviderContext
    {
        MongoClient Client { get; }
    }
}