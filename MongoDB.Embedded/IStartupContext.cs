﻿using MongoDB.Driver;

namespace MongoDB.Embedded
{
    public interface IStartupContext
    {
        MongoServer BaseServer { get; }
        MongoServer ModifiedServer { get; }
        MongoClient Client { get; }
    }
}