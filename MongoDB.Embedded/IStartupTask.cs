﻿namespace MongoDB.Embedded
{
    public interface IStartupTask
    {
        void Execute(IStartupContext context);
    }
}
