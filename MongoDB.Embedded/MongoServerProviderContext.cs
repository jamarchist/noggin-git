﻿using MongoDB.Driver;

namespace MongoDB.Embedded
{
    internal class MongoServerProviderContext : IMongoServerProviderContext
    {
        public MongoClient Client { get; set; }
    }
}