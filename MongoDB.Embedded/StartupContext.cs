﻿using MongoDB.Driver;

namespace MongoDB.Embedded
{
    internal class StartupContext : IStartupContext
    {
        public MongoServer BaseServer { get; set; }
        public MongoServer ModifiedServer { get; set; }
        public MongoClient Client { get; set; }
    }
}