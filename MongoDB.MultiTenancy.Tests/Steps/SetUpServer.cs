﻿using MongoDB.Embedded;
using MongoDB.MultiTenancy.Tests.Utility;
using Tabloid;

namespace MongoDB.MultiTenancy.Tests.Steps
{
    public class SetUpServer : TestStep
    {
        public override void SetUp()
        {
            var mongo = new EmbeddedMongoServer();
            mongo.Settings.StartClean = true;
            mongo.Start();

            var server = new MultiTenantMongoServer(new CollectionDiscriminator(), mongo.GetServer());

            // Add a doc for each tenant
            var tenant1 = server.GetDatabase("tenant-1");
            var docs = tenant1.GetCollection<TestDoc>("docs");
            docs.Save(new TestDoc { Description = "Tenant 1 Test Doc" });

            var tenant2 = server.GetDatabase("tenant-2");
            var docs2 = tenant2.GetCollection<TestDoc>("docs");
            docs2.Save(new TestDoc { Description = "Tenant 2 Test Doc" });

            Test.Resources["mongo"] = mongo;
            Test.Resources["server"] = server;
        }

        public override void TearDown()
        {
            Test.ResourceAs<EmbeddedMongoServer>("mongo").Dispose();
        }
    }
}
