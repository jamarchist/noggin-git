﻿using MongoDB.Bson;

namespace MongoDB.MultiTenancy.Tests.Utility
{
    public class TestDoc
    {
        public ObjectId Id { get; set; }
        public string Description { get; set; }
    }
}
