﻿using System.Linq;
using MongoDB.Bson;
using MongoDB.Driver;
using MongoDB.Driver.Builders;
using MongoDB.Driver.Linq;
using MongoDB.Embedded;
using MongoDB.MultiTenancy.Tests.Steps;
using MongoDB.MultiTenancy.Tests.Utility;
using NUnit.Framework;
using Tabloid.NUnit;

namespace MongoDB.MultiTenancy.Tests
{
    [TestFixture]
    public class when_using_the_collection_discriminator_strategy : ScenarioTest
    {        
        public override void DefineScenario()
        {
            AddStep(new SetUpServer());
        }

        [TestCase("tenant-1")]
        [TestCase("tenant-2")]
        public void queries_return_only_relevant_tenants_results(string tenant)
        {
            var collection = server.GetDatabase(tenant).GetCollection<TestDoc>("docs");
            var results = collection.Find(Query.Matches("Description", new BsonRegularExpression("^Tenant")));

            Assert.AreEqual(1, results.Count());
        }

        [TestCase("tenant-1")]
        [TestCase("tenant-2")]
        public void linq_queries_return_only_relevant_tenants_results(string tenant)
        {
            var collection = server.GetDatabase(tenant).GetCollection<TestDoc>("docs");
            var results = collection.AsQueryable()
                .Where(d => d.Description.StartsWith("Tenant"))
                .ToList();

            Assert.AreEqual(1, results.Count);
        }

        [TestCase("tenant-1")]
        [TestCase("tenant-2")]
        public void entities_appear_to_be_stored_separately(string tenant)
        {
            var collection = server.GetDatabase(tenant).GetCollection<TestDoc>("docs");
            Assert.AreEqual(1, collection.Count());
        }

        [TestCase("tenant-1")]
        [TestCase("tenant-2")]
        public void entities_appear_to_be_stored_separately_as_queryable(string tenant)
        {
            var collection = server.GetDatabase(tenant).GetCollection<TestDoc>("docs").AsQueryable();
            Assert.AreEqual(1, collection.Count());
        }

        [TestCase("tenant-1")]
        [TestCase("tenant-2")]
        public void each_database_has_one_collection_per_entity(string tenant)
        {
            var collections = server.GetDatabase(tenant).GetCollectionNames();
            Assert.AreEqual(1, collections.Count());
        }

        [TestFixtureSetUp]
        public void SetUpScenario()
        {
            mongo = new EmbeddedMongoServer();
            mongo.Settings.StartClean = true;
            mongo.Start();

            server = new MultiTenantMongoServer(new CollectionDiscriminator(), mongo.GetServer());

            // Add a doc for each tenant
            var tenant1 = server.GetDatabase("tenant-1");
            var docs = tenant1.GetCollection<TestDoc>("docs");
            docs.Save(new TestDoc { Description = "Tenant 1 Test Doc" });

            var tenant2 = server.GetDatabase("tenant-2");
            var docs2 = tenant2.GetCollection<TestDoc>("docs");
            docs2.Save(new TestDoc { Description = "Tenant 2 Test Doc" });
        }

        [TestFixtureTearDown]
        public void TearDownScenario()
        {
            mongo.Dispose();
        }



        private EmbeddedMongoServer mongo;
        private MongoServer server;
    }
}
