﻿using System.Linq;
using MongoDB.Driver;
using MongoDB.Embedded;
using MongoDB.MultiTenancy.Tests.Utility;
using NUnit.Framework;

namespace MongoDB.MultiTenancy.Tests
{
    [TestFixture]
    public class when_using_the_collection_per_tenant_strategy
    {
        [TestCase("tenant-1")]
        [TestCase("tenant-2")]
        public void entities_are_stored_separately(string tenant)
        {
            var collection = server.GetDatabase(tenant).GetCollection<TestDoc>("docs");
            Assert.AreEqual(1, collection.Count());
        }

        [TestCase("tenant-1")]
        [TestCase("tenant-2")]
        public void each_database_has_one_collection_per_entity(string tenant)
        {
            var collections = server.GetDatabase(tenant).GetCollectionNames();
            Assert.AreEqual(1, collections.Count());
        }

        [TestFixtureSetUp]
        public void SetUpScenario()
        {
            mongo = new EmbeddedMongoServer();
            mongo.Settings.StartClean = true;
            mongo.Start();

            server = new MultiTenantMongoServer(new CollectionPerTenant(), mongo.GetServer());

            // Add a doc for each tenant
            var tenant1 = server.GetDatabase("tenant-1");
            var docs = tenant1.GetCollection<TestDoc>("docs");
            docs.Save(new TestDoc { Description = "Tenant 1 Test Doc" });

            var tenant2 = server.GetDatabase("tenant-2");
            var docs2 = tenant2.GetCollection<TestDoc>("docs");
            docs2.Save(new TestDoc { Description = "Tenant 2 Test Doc" });
        }

        [TestFixtureTearDown]
        public void TearDownScenario()
        {
            mongo.Dispose();
        }

        private EmbeddedMongoServer mongo;
        private MongoServer server;
    }
}
