﻿using MongoDB.Bson.Serialization;
using MongoDB.Bson.Serialization.Conventions;
using MongoDB.Driver;

namespace MongoDB.MultiTenancy
{
    public class CollectionDiscriminator : IMultiTenancyStrategy
    {
        private static bool conventionsAreRegistered = false;

        public MongoDatabase GetTenantDatabase(MongoServer server, string tenantId)
        {
            return new CollectionDiscriminatorDatabase(server, tenantId, new MongoDatabaseSettings());
        }

        public void RegisterConventions()
        {
            if (conventionsAreRegistered) return;

            var conventions = new ConventionPack();
            conventions.Add(new IgnoreExtraElements());
            ConventionRegistry.Register("CollectionDiscriminatorConventions", conventions, t => true);

            conventionsAreRegistered = true;
        }

        private class IgnoreExtraElements : IClassMapConvention
        {
            public string Name { get { return "IgnoreExtraElements"; } }
            public void Apply(BsonClassMap classMap)
            {
                classMap.SetIgnoreExtraElements(true);
            }
        }
    }
}