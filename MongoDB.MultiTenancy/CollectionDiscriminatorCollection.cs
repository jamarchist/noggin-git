using System;
using MongoDB.Bson;
using MongoDB.Driver;
using MongoDB.Driver.Builders;

namespace MongoDB.MultiTenancy
{
    public class CollectionDiscriminatorCollection<TDocument> : MongoCollection<TDocument>
    {
        private readonly string tenant;

        public CollectionDiscriminatorCollection(MongoDatabase database, string name, MongoCollectionSettings settings, string tenant) : base(database, name, settings)
        {
            this.tenant = tenant;
        }

        public override MongoCursor<TDocument> Find(IMongoQuery query)
        {
            var collection = Database.GetCollection<TDocument>(Name);
            var tenantMatches = Query.EQ("_tenant", new BsonString(tenant));
            if (query == null)
            {
                return collection.Find(tenantMatches);
            }

            var alteredQuery = Query.And(query, tenantMatches);
            return collection.Find(alteredQuery);
        }

        public override MongoCursor FindAs(Type documentType, IMongoQuery query)
        {
            var collection = Database.GetCollection<TDocument>(Name);
            var tenantMatches = Query.EQ("_tenant", new BsonString(tenant));
            // add discriminator
            if (query == null)
            {
                return collection.FindAs(documentType, tenantMatches);
            }

            var alteredQuery = Query.And(query, tenantMatches);
            return collection.FindAs(documentType, alteredQuery);
        }

        public override WriteConcernResult Save(TDocument document)
        {
            var collection = Database.GetCollection<TDocument>(Name);
            var bsonDocument = document.ToBsonDocument(typeof(TDocument));
            bsonDocument.Add("_tenant", new BsonString(tenant));
            return collection.Save(bsonDocument);
        }

        public override long Count()
        {
            var cursor = FindAs(typeof (TDocument), null);
            return cursor.Count();
        }
    }
}