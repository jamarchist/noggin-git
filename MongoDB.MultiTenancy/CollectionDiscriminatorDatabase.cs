using System.Collections.Generic;
using System.Linq;
using MongoDB.Driver;

namespace MongoDB.MultiTenancy
{
    public class CollectionDiscriminatorDatabase : MongoDatabase
    {
        private readonly string tenant;
        private readonly MongoDatabase tenants;

        public CollectionDiscriminatorDatabase(MongoServer server, string name, MongoDatabaseSettings settings) : base(server, name, settings)
        {
            tenant = name;
            tenants = server.GetDatabase("_tenants");
        }

        public override MongoCollection<TDefaultDocument> GetCollection<TDefaultDocument>(string collectionName)
        {
            return new CollectionDiscriminatorCollection<TDefaultDocument>(tenants, collectionName, new MongoCollectionSettings(), tenant);
        }

        public override IEnumerable<string> GetCollectionNames()
        {
            return tenants.GetCollectionNames().Where(n => !n.StartsWith("system"));
        }
    }
}