﻿using MongoDB.Driver;

namespace MongoDB.MultiTenancy
{
    public class CollectionPerTenant : IMultiTenancyStrategy
    {
        public MongoDatabase GetTenantDatabase(MongoServer server, string tenantId)
        {
            return new CollectionPerTenantDatabase(server, tenantId, new MongoDatabaseSettings());
        }

        public void RegisterConventions()
        {
            //
        }
    }
}