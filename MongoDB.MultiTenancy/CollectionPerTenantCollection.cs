using MongoDB.Driver;

namespace MongoDB.MultiTenancy
{
    public class CollectionPerTenantCollection<TDocument> : MongoCollection<TDocument>
    {
        public CollectionPerTenantCollection(MongoDatabase database, string name, MongoCollectionSettings settings) : base(database, name, settings)
        {
        }
    }
}