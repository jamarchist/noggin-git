using System;
using System.Collections.Generic;
using System.Linq;
using MongoDB.Driver;

namespace MongoDB.MultiTenancy
{
    public class CollectionPerTenantDatabase : MongoDatabase
    {
        private readonly string tenant;
        private readonly MongoDatabase tenants;
        private readonly Func<string, string> collectionNameFor;

        public CollectionPerTenantDatabase(MongoServer server, string name, MongoDatabaseSettings settings) : base(server, name, settings)
        {
            tenant = name;
            collectionNameFor = collection => String.Format("{0}-{1}", tenant, collection);
            tenants = server.GetDatabase("_tenants");
        }

        public override MongoCollection<TDefaultDocument> GetCollection<TDefaultDocument>(string collectionName)
        {
            return tenants.GetCollection<TDefaultDocument>(collectionNameFor(collectionName));
        }

        public override IEnumerable<string> GetCollectionNames()
        {
            return tenants.GetCollectionNames().ToList()
                .Where(n => n.StartsWith(String.Format("{0}-", tenant)));
        }
    }
}