﻿using MongoDB.Driver;

namespace MongoDB.MultiTenancy
{
    public class DatabasePerTenant : IMultiTenancyStrategy
    {
        public MongoDatabase GetTenantDatabase(MongoServer server, string tenantId)
        {
            return server.GetDatabase(tenantId);
        }

        public void RegisterConventions()
        {
            // Do nothing
        }
    }
}
