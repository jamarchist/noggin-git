﻿using MongoDB.Driver;

namespace MongoDB.MultiTenancy
{
    public interface IMultiTenancyStrategy
    {
        MongoDatabase GetTenantDatabase(MongoServer server, string tenantId);
        void RegisterConventions();
    }
}
