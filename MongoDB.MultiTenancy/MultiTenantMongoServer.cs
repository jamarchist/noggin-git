﻿using MongoDB.Driver;

namespace MongoDB.MultiTenancy
{
    public class MultiTenantMongoServer : MongoServer
    {
        private readonly IMultiTenancyStrategy multiTenancyStrategy;
        private readonly MongoServer server;

        public MultiTenantMongoServer(IMultiTenancyStrategy multiTenancyStrategy, MongoServer server) : base(server.Settings)
        {
            this.multiTenancyStrategy = multiTenancyStrategy;
            this.server = server;
        }

        public MultiTenantMongoServer(IMultiTenancyStrategy multiTenancyStrategy, MongoServerSettings settings) : base(settings)
        {
            this.multiTenancyStrategy = multiTenancyStrategy;
            this.server = new MongoServer(settings);
        }

        public override MongoDatabase GetDatabase(string databaseName)
        {
            multiTenancyStrategy.RegisterConventions();
            return multiTenancyStrategy.GetTenantDatabase(server, databaseName);
        }
    }
}